﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yopmail_AutomatizationMojix.src.code.control
{
    public class Email
    {
        public Button emailSender = null;
        public Button topicSender = null;

        public void SetEmail(string nameEmail, string topic)
        {
            emailSender = new Button(By.XPath("//body[@class='bodyinbox yscrollbar']/..//span[contains(.,'" + nameEmail + "')]"));
            topicSender = new Button(By.XPath("//body[@class='bodyinbox yscrollbar']/..//div[contains(.,'" + topic + "') and @class='lms']"));
        }

        public bool TheEmailExist()
        {
            bool result = true;
            if (emailSender.IsControlDisplayed() == false)
                result = false;
            else if (topicSender.IsControlDisplayed() == false)
                result = false;

            return result;
        }
    }
}
