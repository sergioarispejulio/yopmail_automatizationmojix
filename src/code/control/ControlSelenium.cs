﻿using Yopmail_AutomatizationMojix.src.code.session;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yopmail_AutomatizationMojix.src.code.control
{
    public class ControlSelenium
    {
        protected By locator;
        protected IWebElement control;

        public ControlSelenium(By locator)
        {
            this.locator = locator;

        }

        public void FindControl()
        {
            control = Session.Instance().GetBrowser().FindElement(locator);
        }

        public IWebElement GetIWebElement()
        {
            return control;
        }

        public void Click()
        {
            FindControl();
            control.Click();
        }

        public Boolean IsControlDisplayed()
        {
            try
            {
                FindControl();
                return control.Displayed;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

}
