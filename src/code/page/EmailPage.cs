﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yopmail_AutomatizationMojix.src.code.control;

namespace Yopmail_AutomatizationMojix.src.code.page
{
    public class EmailPage
    {
        public Iframe messageFrame = new Iframe(By.XPath("//iframe[@name='ifmail']"));
        public Button logoPageButton = new Button(By.XPath("//a[@class='wmlogoclick']"));

        public Button refreshButton = new Button(By.Id("refresh"));
        public Button newEmailButton = new Button(By.Id("newmail"));

        //public TextBox nameSendEmail = new TextBox(By.XPath("//div[@class='tooltip click']/..//input[@id='msgto']"));
        public TextBox nameSendEmail = new TextBox(By.Id("msgto"));
        //public TextBox topicEmail = new TextBox(By.XPath("//div[@class='inputsend']/input[@id='msgsubject']"));
        public TextBox topicEmail = new TextBox(By.Id("msgsubject"));
        public TextArea contentEmail = new TextArea(By.XPath("//div[@id='msgbody']"));
        public Button buttonSendEmail = new Button(By.XPath("//button[@id='msgsend']"));
        public Button successMessage = new Button(By.XPath("//div[@id='msgpop']/div/div[contains(.,'Tu mensaje ha sido enviado')]"));

        public Email message = new Email();

        public bool SetEmailToFind(string sender, string topic)
        {
            bool result;
            message.SetEmail(sender, topic);
            result = message.TheEmailExist();
            return result;
        }

        public void ChangeToMessageIframe()
        {
            messageFrame.FindControl();
            session.Session.Instance().GetBrowser().SwitchTo().Frame(messageFrame.GetIWebElement());
        }

        public void ReturntoMainFrame()
        {
            session.Session.Instance().GetBrowser().SwitchTo().DefaultContent();
        }

    }
}
