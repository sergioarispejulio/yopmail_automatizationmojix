﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yopmail_AutomatizationMojix.src.code.control;

namespace Yopmail_AutomatizationMojix.src.code.page
{
    public class MainPage
    {
        public TextBox nameEmail = new TextBox(By.XPath("//input[@class='ycptinput']"));
        public Button confirmEmail = new Button(By.XPath("//button[@class='md']"));
        public Button cancelEmail = new Button(By.XPath("//div[@id='clearbut']/a/i"));
    }
}
