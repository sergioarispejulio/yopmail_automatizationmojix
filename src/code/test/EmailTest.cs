﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using Yopmail_AutomatizationMojix.src.code.page;

namespace Yopmail_AutomatizationMojix.src.code.test
{
    [TestClass]
    public class EmailTest : TestBase
    {
        MainPage mainPage = new MainPage();
        EmailPage emailPage = new EmailPage();

        string nameSendEmail = "pruebaqamojixenviar@yopmail.com";
        string nameReciveEmail = "pruebaqamojixrecibir@yopmail.com";
        string email1 = "pruebaqamojixenviar";
        string email2 = "pruebaqamojixrecibir";
        string topic1 = "Prueba 1";
        string conten1 = "Contenido 1";

        [TestMethod]
        public void Test1_SendEmail()
        {
            bool condition;
            login(email1);
            emailPage.newEmailButton.Click();
            Thread.Sleep(1000);
            emailPage.ChangeToMessageIframe();
            Thread.Sleep(1000);

            emailPage.nameSendEmail.SetText(nameReciveEmail);
            emailPage.topicEmail.SetText(topic1);
            emailPage.contentEmail.SetText(conten1);
            emailPage.buttonSendEmail.Click();
            Thread.Sleep(1000);

            condition = emailPage.successMessage.IsControlDisplayed();
            emailPage.ReturntoMainFrame();
            Thread.Sleep(1000);
            Assert.IsTrue(condition, "ERROR !! Email wasn't send");
        }

        [TestMethod]
        public void Test2_CheckEmailsended()
        {
            bool condition;
            login(email2);
            Thread.Sleep(1000);

            emailPage.message.SetEmail(topic1, conten1);
            condition = emailPage.message.TheEmailExist();
            Assert.IsTrue(condition, "ERROR !! The receptor won't recibe the email");
        }


        private void login(string email)
        {
            mainPage.nameEmail.SetText(email);
            mainPage.confirmEmail.Click();
            Thread.Sleep(100);
        }
    }
}
